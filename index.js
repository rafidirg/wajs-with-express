const { Client, LocalAuth, MessageMedia } = require('whatsapp-web.js');
const express = require("express");
const bodyParser = require("body-parser");
const qrcode = require('qrcode-terminal');

const client = new Client({
  authStrategy: new LocalAuth(),
});

const app = express();
const jsonParser = bodyParser.json()

client.on('qr', qr => {
  qrcode.generate(qr, { small: true });
});

client.on('ready', () => {
  console.log('Client is ready!');
  app.listen(3000, () => {
    console.log("Server running on port 3000");
  });
});

client.initialize();
console.log("Initializing...");

client.on('message', message => {
  if (message.body === '!ping') {
    client.sendMessage(message.from, 'pong');
    console.log(message.from)
  }
});

app.post("/:phoneNumber", jsonParser, async (req, res) => {
  console.log(req.body)
  try {
    await client.sendMessage(`${req.params.phoneNumber}@c.us`, req.body.message)
    res.status(200).send("Success")
  } catch (err) {
    console.error(err)
    res.status(500).send("Internal Server Error")
  }
});

app.post("/send-attachment/:phoneNumber", jsonParser, async (req, res) => {
  console.log("Send message with attachment");
  try {
    const media = MessageMedia.fromFilePath(req.body.media)
    await client.sendMessage(`${req.params.phoneNumber}@c.us`, media)
    await client.sendMessage(`${req.params.phoneNumber}@c.us`, req.body.message)
    res.status(200).send("success")
  } catch (err) {
    console.error(err)
    res.status(500).send("Internal Server Error")
  }
})
